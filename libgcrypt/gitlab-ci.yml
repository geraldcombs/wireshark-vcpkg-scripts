# Repackage libgcrypt from https://packages.msys2.org/base/mingw-w64-libgcrypt
# Adapted from https://git.lekensteyn.nl/peter/wireshark-notes/tree/windows-libs/make-libgcrypt-libs-zip.sh

libgcrypt:
  image: debian:stable
  stage: build
  variables:
    PKG_VERSION: "1.10.1-2"
  rules:
  - if: '$CI_PIPELINE_SOURCE == "push"'
    when: manual
    allow_failure: true
  - if: '$CI_PIPELINE_SOURCE == "push"'
    changes:
      - libgcrypt
  script:
    - version=1.10.2-2
    - DEBIAN_FRONTEND=noninteractive apt-get update
    - DEBIAN_FRONTEND=noninteractive apt-get --yes install binutils curl zip zstd
    # Use MSYS2's CLANG64 and CLANGARM64 since they support the architectures we need and they link with the UCRT
    # https://www.msys2.org/docs/environments/
    - |
      urls=(
        https://mirror.msys2.org/mingw/clangarm64/mingw-w64-clang-aarch64-libgcrypt-1.10.2-1-any.pkg.tar.zst
        https://mirror.msys2.org/mingw/clang64/mingw-w64-clang-x86_64-libgcrypt-1.10.2-1-any.pkg.tar.zst
        https://mirror.msys2.org/mingw/clangarm64/mingw-w64-clang-aarch64-libgpg-error-1.47-1-any.pkg.tar.zst
        https://mirror.msys2.org/mingw/clang64/mingw-w64-clang-x86_64-libgpg-error-1.47-1-any.pkg.tar.zst
      )
    - for url in "${urls[@]}"; do curl -JLO $url ; done
    - |
      sha256sum --check <<SHA256
      2a97f11a572dc79a4f3fb453fe76edacaaf6bcb36551792d4c16448341244b76  mingw-w64-clang-aarch64-libgcrypt-1.10.2-1-any.pkg.tar.zst
      985510e047826c6b2f6ffe22b18ced9413e105c2e8d15eafeb77197b2b1c68ba  mingw-w64-clang-aarch64-libgpg-error-1.47-1-any.pkg.tar.zst
      1b16e38bb3e5aef8a9f3018f2e92db52f5aa6ef4775441926a6fb2661549bf27  mingw-w64-clang-x86_64-libgcrypt-1.10.2-1-any.pkg.tar.zst
      fc834534dc540f2fe28f97d9fe0b8bbfce19db9f82dd08e206ae60bd3d893b6f  mingw-w64-clang-x86_64-libgpg-error-1.47-1-any.pkg.tar.zst
      SHA256
    - for _f in mingw-w64-*.tar.zst ; do zstdcat $_f | tar -xf -; done
    # Don't redefine ssize_t
    - sed 's,typedef long ssize_t;,/*&*/,' -i clang*/include/gcrypt.h
    - |
      for prefix in clang* ; do
        # Use vcpkg's triplet names
        case $prefix in
          clang64) destdir=libgcrypt-$version-x64-mingw-dynamic-ws ;;
          clangarm64) destdir=libgcrypt-$version-arm64-mingw-dynamic-ws ;;
          *) continue ;;
        esac
        rm -rf "$destdir" "$destdir.zip"
        mkdir --mode=755 "$destdir" "$destdir/bin" "$destdir/lib"
        cp -va "$prefix/include" "$destdir/"
        cp -va "$prefix/lib/libgcrypt.dll.a" "$destdir/lib/libgcrypt-20.lib"
        cp -va "$prefix/lib/libgpg-error.dll.a" "$destdir/lib/libgpg-error-0.lib"
        cp -va "$prefix/bin/"*.dll "$destdir/bin/"
        # Not needed?
        # https://github.com/msys2/MINGW-packages/blob/master/mingw-w64-libgcrypt/PKGBUILD
        # strip "$destdir/bin/"*.dll
        {
          echo "Downloaded from MSYS2:"
          printf "%s\n" "${urls[@]}"
          printf "\nOther comments:\n"
          echo "- libgcrypt was compiled with the --disable-asm flags"
          echo "- the ssize_t typedef was commented out in gcrypt.h; we define it elsewhere"
          echo "- lib/*.dll.a files were moved to bin/*.lib (including a version number based on the lib/*.dll file)"
        } | sed 's/$/\r/' > "$destdir/README.Wireshark"
        # Create zip, but without extra info such as timestamp and uid/gid (-X)
        zip -Xr "$destdir.zip" "$destdir"
      done
    - ls -l libgcrypt-*-ws.zip
    - sha256sum libgcrypt-*-ws.zip
  artifacts:
    paths:
      - ${PORT_NAME}-*.zip
  needs: []
